# Copyright (C) 2015-2019 |Meso|Star> (contact@meso-star.com)
#
# This software is a computer program whose purpose is to help in the
# setup of the Star-Engine devel.
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software. You can use,
# modify and/or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty and the software's author, the holder of the
# economic rights, and the successive licensors have only limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading, using, modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean that it is complicated to manipulate, and that also
# therefore means that it is reserved for developers and experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and, more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

if(CMAKE_HOST_UNIX)
  set(CP cp -d)
  set(CPDIR cp -rd)
else()
  set(CP ${CMAKE_COMMAND} -E copy)
  set(CPDIR ${CMAKE_COMMAND} -E copy_directory)
endif()

execute_process(COMMAND ${CMAKE_COMMAND} -E remove_directory _Pkg)
execute_process(COMMAND ${CPDIR} ${STAR_INSTALL_PREFIX} _Pkg)

# Copy the system runtimes
if(CMAKE_INSTALL_SYSTEM_RUNTIME_LIBS)
  execute_process(COMMAND ${CP} ${CMAKE_INSTALL_SYSTEM_RUNTIME_LIBS} _Pkg/bin)
endif()

execute_process(COMMAND cpack --config ${CMAKE_CURRENT_BINARY_DIR}/CPackConfig.cmake)
