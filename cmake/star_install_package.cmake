# Copyright (C) 2015-2019 |Meso|Star> (contact@meso-star.com)
#
# This software is a computer program whose purpose is to help in the
# setup of the Star-Engine devel.
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software. You can use,
# modify and/or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty and the software's author, the holder of the
# economic rights, and the successive licensors have only limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading, using, modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean that it is complicated to manipulate, and that also
# therefore means that it is reserved for developers and experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and, more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

# Manage the install/update of a tgz package. This script relies on the
# following required variables
#   - STAR_PKG_INSTALL_PREFIX: where the package is installed ;
#   - STAR_PKG_NAME: the name of the package;
#   - STAR_PKG_REPOSITORY: the source of the binary package;
#   - STAR_PKG_SIZEOF_VOID_P: size in bytes of client pointers
#   - STAR_PKG_VERSION: the version of the package.
cmake_minimum_required(VERSION 2.8)

################################################################################
# Check script arguments
################################################################################
function(check_arg _arg)
  if("${${_arg}}" STREQUAL "")
    message(FATAL_ERROR "Missing the `${_arg}' variable")
  endif()
endfunction()
check_arg(STAR_PKG_INSTALL_PREFIX)
check_arg(STAR_PKG_NAME)
check_arg(STAR_PKG_REPOSITORY)
check_arg(STAR_PKG_SIZEOF_VOID_P)
check_arg(STAR_PKG_VERSION)

################################################################################
# Define the scp port
################################################################################
string(REGEX MATCH "^ssh://.+:[0-9]+/.*$" REPO_PORT "${STAR_PKG_REPOSITORY}")
if(NOT REPO_PORT)
  set(REPO_PORT 22)
else()
  string(REGEX REPLACE "^ssh://.+:([0-9]+)/.*$" "\\1" REPO_PORT "${STAR_PKG_REPOSITORY}")
  string(REGEX REPLACE "^ssh://(.+):[0-9]+/(.*)$" "\\1:\\2" STAR_PKG_REPOSITORY "${STAR_PKG_REPOSITORY}")
endif()

################################################################################
# Define some local variables
################################################################################
# Define the real binary package name with respect to the host
if(NOT STAR_PKG_SIZEOF_VOID_P EQUAL 8)
   message(FATAL_ERROR "Unsupported addressing")
endif()

if(CMAKE_HOST_WIN32)
  set(_platform win64)
elseif(CMAKE_HOST_UNIX)
  set(_platform gnu_linux64)
else()
  message(FATAL_ERROR "Unsupported platform")
endif()

set(_pkg_name ${STAR_PKG_NAME}_${_platform})
set(_pkg_md5sum ${STAR_PKG_NAME}_${STAR_PKG_VERSION}_${_platform}.md5sum)
set(_pkg_md5sum_new .${_pkg_md5sum})
set(_pkg ${STAR_PKG_NAME}_${STAR_PKG_VERSION}_${_platform}.tgz)

find_file(_file_md5sum ${_pkg_md5sum} PATHES ${STAR_PKG_INSTALL_PREFIX}/share/star-package)
find_file(_pkg_uninstall ${STAR_PKG_NAME}_${STAR_PKG_VERSION}_${_platform}_uninstall.cmake
  PATHES ${STAR_PKG_INSTALL_PREFIX}/share/star-package)

################################################################################
# Package management
################################################################################
function(download _url _dst)
  file(DOWNLOAD "${_url}" "${_dst}" STATUS _status)
  list(GET _status 0 _err)
  if(_err)
    list(GET _status 1 _err)
    message(FATAL_ERROR "Download ${_url} -> ${_dst}\n error: ${_err}")
  endif()
endfunction()

execute_process(COMMAND ${CMAKE_COMMAND} -E make_directory ${STAR_PKG_INSTALL_PREFIX})
execute_process(COMMAND ${CMAKE_COMMAND} -E make_directory ${STAR_PKG_INSTALL_PREFIX}/share/star-package)
download(${STAR_PKG_REPOSITORY}/${_pkg_md5sum} ${STAR_PKG_INSTALL_PREFIX}/share/star-package/${_pkg_md5sum_new})

file(STRINGS ${STAR_PKG_INSTALL_PREFIX}/share/star-package/${_pkg_md5sum_new} _md5sum_new)

if(_file_md5sum)
  file(STRINGS ${_file_md5sum} _md5sum_old)
endif()

if(_file_md5sum AND "${_md5sum_old}" STREQUAL "${_md5sum_new}")
  message(STATUS "The `${_pkg_name}' package is up to date")
  file(REMOVE ${_pkg_md5sum_new})
else()
  if(_pkg_uninstall)
    message(STATUS "Uninstall the previous `${_pkg_name}' package")
    execute_process(COMMAND ${CMAKE_COMMAND} -P ${_pkg_uninstall}
      ERROR_VARIABLE _err
      WORKING_DIRECTORY ${STAR_PKG_INSTALL_PREFIX})
    if(_err)
      message(FATAL_ERROR ${_err})
    endif()
  endif()

  message(STATUS "Download the `${_pkg_name}' package")
  download(${STAR_PKG_REPOSITORY}/${_pkg}
     ${STAR_PKG_INSTALL_PREFIX}/share/star-package/${_pkg})

  file(MD5 ${STAR_PKG_INSTALL_PREFIX}/share/star-package/${_pkg} _md5sum_tmp)

  string(REGEX MATCH "^[a-fA-F0-9]*" _md5sum_new_wo_filename ${_md5sum_new})
  if(NOT "${_md5sum_tmp}" STREQUAL "${_md5sum_new_wo_filename}")
    message(STATUS "The downloaded file `${_pkg}' is corrupted")
  endif()

  message(STATUS "Install the `${_pkg_name}' package")
  execute_process(
    COMMAND ${CMAKE_COMMAND} -E tar xzvf share/star-package/${_pkg}
    ERROR_VARIABLE _err
    WORKING_DIRECTORY ${STAR_PKG_INSTALL_PREFIX})
  if(_err)
    message(FATAL_ERROR ${_err})
  endif()

  message(STATUS "Update the local `${_pkg_name}' md5sum")
  execute_process(
    COMMAND ${CMAKE_COMMAND} -E copy ${_pkg_md5sum_new} ${_pkg_md5sum}
    ERROR_VARIABLE _err
    WORKING_DIRECTORY ${STAR_PKG_INSTALL_PREFIX}/share/star-package)
  if(_err)
    message(FATAL_ERROR ${_err})
  endif()

  message(STATUS "Cleanup temporary `${_pkg_name}' files")
  execute_process(
    COMMAND ${CMAKE_COMMAND} -E remove ${_pkg_md5sum_new} ${_pkg}
    ERROR_VARIABLE _err
    WORKING_DIRECTORY ${STAR_PKG_INSTALL_PREFIX}/share/star-package)
  if(_err)
    message(FATAL_ERROR ${_err})
  endif()
endif()

