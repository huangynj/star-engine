#!/bin/bash

p=$(dirname $(dirname $(realpath $BASH_SOURCE)))

export LD_LIBRARY_PATH=$p/lib:${LD_LIBRARY_PATH}
export PATH=$p/bin:${PATH}
export MANPATH=$p/share/man/:${MANPATH}
export CPATH=$p/include:${CPATH}
export LIBRARY_PATH=$p/lib:${LIBRARY_PATH}
