# Star Engine

## Overview

The purpose of Star-Engine is to provide a development environment for
engineers and researchers who wish to use the Monte-Carlo method in order to
perform numerical simulations. The Monte-Carlo method is indeed the only
numerical method suitable to solve high-dimensional integrals even if an higly
complex 3D scene or many coupled phenomena have to be considered.

On paper, such complex situations are easily handled: physicists will
essentially translate the integral they need to solve into a mathematical form
that can be read as a Monte-Carlo algorithm. But then, developing the
associated simulation code might be very difficult, if even possible, because
mathematical expressions do not provide any clue on data management concerns,
or on how to address the development environment constraints. One good example
would be a common integral over a surface: while it could be quite easy to
write this integral, coding a fast and accurate function that can be used for
any given surface is no small feat.

Star-Engine provides such practical solutions: it can be seen as a bridge
between a Monte-Carlo algorithm that exists only as a mathematical expression
and the actual numerical simulation code that uses this algorithm in order to
evaluate the solution.

Several applications are currently based on the Star-Engine framework for
several purposes ranging from radiative transfer to electromagnetism. Refer to
the their associated documentation for more informations:

  * [High-Tune: RenDeRer](https://gitlab.com/meso-star/htrdr.git)
  * [Schiff](https://gitlab.com/meso-star/schiff.git)
  * [Solstice](https://gitlab.com/meso-star/solstice.git)
  * [Star-4V/S](https://gitlab.com/meso-star/star-4v_s.git)
  * [Star-GebhartFactor](https://gitlab.com/meso-star/star-gf.git)

## Components

The Star-Engine is a collection of C libraries and programs available for the
x86-64 architecture on GNU/Linux and Microsoft Windows 7 and later. The core
components of the Star-Engine framework are briefly described below:

- [Star-Sampling](https://gitlab.com/meso-star/star-sp.git)  provides a set of
  [Pseudo] Random Number Generators and random variates useful in the
  development of Monte-Carlo solvers which heavily rely on distributions of
  random variables.
- [Star-MonteCarlo](https://gitlab.com/meso-star/star-mc.git) simplifies the
  development of efficient Monte-Carlo solvers by providing multi-threaded
  integration of user defined integrands.
- [Star-2D](https://gitlab.com/meso-star/star-2d.git) and
  [Star-3D](https://gitlab.com/meso-star/star-3d.git) provide functionalities
  to define a complex virtual environment composed of a set of 2D/3D geometries
  that can then be ray-traced and sampled both robustly and efficiently. These
  functionalities are particularly well suited for solvers that have to deal
  with arbitrary 2D/3D shapes (e.g. radiative or diffusive random walks, etc.).
- [Star-3DAW](https://gitlab.com/meso-star/star-3daw.git) and
  [Star-3DSTL](https://gitlab.com/meso-star/star-3dstl.git) create Star-3D
  shapes from geometries saved, respectively, in the
  [Alias Wavefront.obj](https://en.wikipedia.org/wiki/Wavefront_.obj_file) and
  [STereo Lithography](https://en.wikipedia.org/wiki/STL_(file_format)) file
  formats.
- [Star-3DUT](https://gitlab.com/meso-star/star-3dut.git) generates triangular
  meshes for several geometric shapes (cuboid, cylinder, sphere, etc.).
- [Star-VoXel](https://gitlab.com/meso-star/star-vx.git) manages volume
  elements hierarchically structured as a set of axis aligned cuboids, that can
  then be efficiently accessed or queried through ray-tracing.
- [Star-ScatteringFunctions](https://gitlab.com/meso-star/star-sf.git) defines
  abstractions to describe how the light is scattered at a surface.

Beside the aforementionned libraries, the
[Star-Display](https://gitlab.com/meso-star/star-display.git) program can be
installed: it draw geometries saved with respect to the Alias Wavefront .obj or
STereo Lithography file formats.

## Install from sources

The Star-Engine framework is compatible GNU/Linux as well as Microsoft Windows
7 and later.  Note that only x86 CPUs and 64-bits operating systems are
supported. The Star-Engine repository provides a [CMake](https://cmake.org)
script used to build the "Star projects" from their source tree. On its
execution, it compiles and deploys the selected projects as well as their
dependencies. In addition of binaries, libraries headers and CMake packages are
also installed, providing the whole environment required to develop
"Star-Engine based" softwares.

### Pre-requisites

The Star-Engine build script relies on the [CMake](https://cmake.org) build
system as well as on the [git](https://git-scm.com) source control. Since
several projects are directly built from sources, a C and C++ compiler -
compatible with the C99 and C++11 standards - must also be installed on the
system (e.g. [GNU Compiler Collection](https://gcc.gnu.org) 4.7 or later).

### Build

Use git to clone the Star-Engine repository on the target machine. Then,
generate the CMake project from the `CMakeLists.txt` file stored in the `cmake`
directory (Refer to the [CMake documentation](https://cmake.org/documentation)
for further informations on CMake). At this step, one can use the CMake user
interface to configure the Star-Engine installation (e.g. list of projects to
deploy, install destination, etc.). Finally, build the resulting project. On
build invocation, the Star-Engine downloads, potentially builds, and installs
the Star-Engine projects as well as their dependencies in the
`CMAKE_INSTALL_PREFIX` directory. By default, the install destination is the
`local` sub-directory of the Star-Engine.


On a GNU/Linux operating system this can be sum up to the following command
lines:

    ~ $ git clone https://gitlab.com/meso-star/star-engine.git
    ~ $ mkdir star-engine/build && cd star-engine/build
    ~/star-engine/build $ cmake ../cmake && make
    ~/star-engine/build $ ls -lR ../local | more

On a Microsoft Windows operating system, first ensure that the directories of
the git and the cmake programs are registered into the `path` environment
variable. Then open a command prompt and execute the commands below. Note that
the following example assumes that Visual Studio 2015 is installed on the
system:

    C:\Users\Meso-Star>git clone https://gitlab.com/meso-star/star-engine.git
    C:\Users\Meso-Star>mkdir star-engine\build && cd star-engine\build
    C:\Users\Meso-Star\star-engine\build>cmake ..\cmake -G "Visual Studio 14 2015 Win64"
    C:\Users\Meso-Star\star-engine\build>cmake --build . --config Release
    C:\Users\Meso-Star\star-engine\build>dir /S ..\local | more

### Update

To update the overall Star-Engine framework, first check that the Star-Engine
repository is up to date. Then force its rebuild to update the outdated
projects.

On GNU/Linux:

    ~/star-engine/build $ git pull origin master
    ~/star-engine/build $ make clean && make

On Microsoft Windows:

    C:\Users\Meso-Star\star-engine\build>git pull origin master
    C:\Users\Meso-Star\star-engine\build>cmake --build . --config Release --target clean
    C:\Users\Meso-Star\star-enigne\build>cmake --build . --config Release


### CMake configuration

The default CMake configuration of the Star-Engine build script should be
appropriate for most usages. Anyway, this section lists the parameters that can
be configured to tune its deployment:

- `CMAKE_BUILD_TYPE`: define how the Star-Engine projects are built. The valid
  types are `Debug`, `Release` or `RelWithDebInfo`. The latter type is
  equivalent to the release mode excepted that code assertions and debug
  symbols are enabled as in debug mode. By default, `CMAKE_BUILD_TYPE` is set
  to `Release`. Note that this variable is used only by static build systems as
  GNU/Makefile; when built on Windows with a regular Visual Studio project, one
  have to use the `--target` option of the `cmake` command line to switch
  between the aforementioned modes.
- `CMAKE_INSTALL_RPATH_USE_LINK_PATH`: when enabled, forces the compiled
  projects to explicitly rely on the libraries used during the build process.
  By default, its value is `OFF` and, consequently, the dependencies are
  resolved at runtime; for instance, on GNU/Linux, they are searched in the
  directories listed in the `LD_LIBRARY_PATH` environment variable.
- `DEPLOY_STAR_2D`: enable the installation of the
  [Star-2D](https://gitlab.com/meso-star/star-2d.git) library.
  By default it is set to `ON`.
- `DEPLOY_STAR_3DAW`: enable the installation of the
  [Star-3DAW](https://gitlab.com/meso-star/star-3daw.git) library.
  By default it is set to `ON`.
- `DEPLOY_STAR_3DSTL`: enable the installation of the
  [Star-3DSTL](https://gitlab.com/meso-star/star-3dstl.git) library.
  By default it is set to `ON`.
- `DEPLOY_STAR_3DUT`: enable the installation of the
  [Star-3DUtilityToolkit](https://gitlab.com/meso-star/star-3dut.git) library.
  By default it is set to `ON`.
- `DEPLOY_STAR_DISPLAY`: enable the installation of the
  [Star-Display](https://gitlab.com/meso-star/star-display.git) program.
  By default it is set to `ON`.
- `DEPLOY_STAR_MC`: enable the installation of the
  [Star-MonteCarlo](https://gitlab.com/meso-star/star-mc.git) library.
  By default it is set to `ON`.
- `DEPLOY_STAR_RDR`: enable the installation of the
  [Star-Renderer](https://gitlab.com/meso-star/star-rdr.git) library.
  By default it is set to `ON`.
- `DEPLOY_STAR_SF`: enable the installation of the
  [Star-ScatteringFunctions](https://gitlab.com/meso-star/star-sf.git) library.
  By default it is set to `ON`.
- `DEPLOY_STAR_VX`: enable the installation of the
  [Star-VoXel](https://gitlab.com/meso-star/star-vx.git) library.
  By default it is set to `ON`.
- `EMBREE_VERSION`: define the version of the
  [Embree](https://embree.github.io/) ray-tracing library to use. Currently,
  the only available version is `3.3`.
- `REPO_STAR`: URL toward the public repository of |Meso|Star> that hosts the
  sources of the Star-Engine projects. Its default value is
  `https://gitlab.com/meso-star/`.
- `REPO_VAPLV`: URL of a repository that, among others, hosts the sources of
  the [RCMake](https://gitlab.com/vaplv/rcmake.git) and
  [RSys](https://gitlab.com/vaplv/rsys.git) projects used by almost all
  the Star-Engine projects. Its default value is `https://gitlab.com/vaplv/`.
- `REPO_PKG`: URL of the repository that hosts pre-compiled dependencies used
  by the Star-Engine as, for instance, the Embree library. Its default value is
  `https://www.meso-star.com/packages/v0.3/`.
- `STAR_INSTALL_PREFIX`: define the install directory of the Star-Engine. By
  default, it is installed in the `local` sub-directory of the Star-Engine
  project.

## Develop with the Star-Engine & CMake

Once installed, the Star-Engine provides [CMake
packages](https://cmake.org/cmake/help/v3.5/manual/cmake-packages.7.html) that
help programmers to use its libraries in their softwares relying on the CMake
build-system. To use these packages, at the CMake configuration step of the
software, add the Star-Engine CMake package location as well as its install
directory to the CMake search paths. Then use the CMake package mechanism to
use the required Star-Engine libraries.

For instance, let a project relying on the
[Star-3D](https://gitlab.com/meso-star/star-3d.git) and the
[Star-SamPling](https://gitlab.com/meso-star/star-sp.git) libraries, with a
single source file `main.c` and a `CMakeLists.txt` file defined as follow:

    project(my_project C)

    # Use CMake packages to check and add project dependencies
    find_package(Star3D REQUIRED)
    find_package(StarSP REQUIRED)
    include_directories(${Star3D_INCLUDE_DIR} ${StarSP_INCLUDE_DIR})

    # Define the program to build
    add_executable(my_program main.c)

    # Link the program against the Star-Engine libraries on which it depends
    target_link_libraries(my_program StarSP Star3D)

On a GNU/Linux system, one can generate an Unix Makefile of this project
with the following command:

    cmake -G "Unix Makefiles" -DCMAKE_PREFIX_PATH="<STAR_ENGINE_DIR>" <MY_PROJECT_DIR>

On a Microsoft Windows operating system, first ensure that the directory of the
cmake program is registered into the `path` environment variable. Then execute
the following command to generate a Visual Studio 2015 solution of this
project:

    cmake -G "Visual Studio 14 2015 Win64" -DCMAKE_PREFIX_PATH="<STAR_ENGINE_DIR>" <MY_PROJECT_DIR>

with `<STAR_ENGINE_DIR>` the install location of the Star-Engine and
`<MY_PROJECT_DIR>` the directory where the aforementioned `CMakeLists.txt` file
is stored.

## Release notes

### Version 0.8

- Bump Embree version to 3.3
- Migrate the ray-tracing backend of the
  [Star-2D](https://gitlab.com/meso-star/star-2d.git) and the
  [Star-3D](https://gitlab.com/meso-star/star-3d.git) libraries from Embree2 to
  Embree3.

### Version 0.7

- Add the [Star-2D](https://gitlab.com/meso-star/star-2d.git) library. This
  library manages two dimensional scenes composed of line segments that can be
  then sampled and ray-traced. It is actually the counterpart of the Star-3D
  library for the 2D geometries
- Add the [Star-VoXel](https://gitlab.com/meso-star/star-vx.git) library that
  manages a set of volume elements (i.e. voxels) structured as a set of axis
  aligned cuboids. Star-VoXel also provides efficient ways to index voxels or
  access them through ray-tracing.
- Add support of phase functions to the
  [Star-ScatteringFunction](https://gitlab.com/meso-star/star-sf.git) library
  to describe the angular distribution of light reflected from a body when
  illuminated from a specific direction.
- Add to [Star-SamPling](https://gitlab.com/meso-star/star-sp.git) the uniform
  sphere cap random variate.
- Fix several issues in several libraries: refer to the release notes of the
  projects for more informations.

### Version 0.6

- Update the Embree library to 2.17.
- Update several Star-Engine projects. Most notably,
  [Star-3D](https://gitlab.com/meso-star/star-3d.git),
  [Star-MonteCarlo](https://gitlab.com/meso-star/star-mc.git) and
  [Star-SamPling](https://gitlab.com/meso-star/star-sp.git) are updated to the
  version 0.5, 0.4 and 0.7, respectively. Refer to their release note for more
  informations.
- Update the default URL of the pre-compiled dependencies to `HTTP secure`.

### Version 0.5.1

- Update the `star-engine.profile` script: setup the `CPATH` and
  `LIBRARY_PATH` environment variable.

### Version 0.5

- Update boost dependency on Windows to 1.64.
- Update rsys dependency to 0.5.
- Update Star-Engine projects to their latest available versions (s3dut 0.2, ssf
  0.4, ssp 0.5); this introduces API discontinuities!

### Version 0.4

- Update of almost all Star-Engine projects; among others, bump version of the
  [Star-3D](https://gitlab.com/meso-star/star-3d.git) and
  [Star-SamPling](https://gitlab.com/meso-star/star-sp.git) projects to 0.4.1
  and 0.4, respectively. Both are incompatible with their previous version.
- Add the [Star-ScatteringFunctions](https://gitlab.com/meso-star/star-sf.git)
  library: it provides abstraction to manage how light is scattered at an
  interface.
- Add the [Star-3DUT](https://gitlab.com/meso-star/star-3dut.git) library: it
  generates triangular meshes for several common shapes (cuboid, cylinder,
  sphere, etc.).
- Remove the optional [Schiff](https://gitlab.com/meso-star/schiff.git) and
  [Star-GebhartFactor](https://gitlab.com/meso-star/star-gf.git) projects from
  the Star-Engine main script; they have their own Star-Engine script on the
  `schiff` and `gebhart` branches, respectively.

## Licenses

Copyright (C) 2015-2019 |Meso|Star> (<contact@meso-star.com>). Star-Engine is
free software released under the CeCILL v2.1 license. You are welcome to
redistribute it under certain conditions; refer to the COPYING files for
details.

The licenses of the projects installed by the Star-Engine build script are
deployed in the `share/doc` sub-directory of the Star-Engine install path.
Refer to the documentation of each project for more informations on their
associated license.
